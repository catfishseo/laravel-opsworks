node[:deploy].each do |application, deploy|
  script "install_composer" do
    interpreter "bash"
    user "root"
    cwd "#{deploy[:deploy_to]}/current"

    code <<-EOH
    php artisan tdc:install
    EOH

    only_if { ::File.exist?("#{deploy[:deploy_to]}/current/artisan.php")}
  end
end