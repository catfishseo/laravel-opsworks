name "services"
maintainer "Leandro Papasidero"
maintainer_email "leandrop.papasidero@creditcards.com"
license "MIT"
description "Installs Composer and project dependencies"
version "0.0.1"

depends "ntp", "~> 1.8.2"