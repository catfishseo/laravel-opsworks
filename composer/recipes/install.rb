node[:deploy].each do |application, deploy|
  script "install_composer" do
    interpreter "bash"
    user "root"
    cwd "#{deploy[:deploy_to]}/current"
    code <<-EOH
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
    composer install --no-dev --no-interaction --prefer-dist
    EOH
    only_if { ::File.exist?("#{deploy[:deploy_to]}/current/composer.json")}
  end
end